﻿using System;

namespace Task06
{
    class Program
    {
        static void Main(string[] args)
        {
            {
                //get user input
                Console.WriteLine("Please write weight and height");
                Console.Write("Enter Weight :");

                int width = Convert.ToInt32(Console.ReadLine());
                Console.Write("Enter Height :");

                int height = Convert.ToInt32(Console.ReadLine());

                //calculate Bmi and return result
                BMIService bmiService = new BMIService(width, height);
                Console.WriteLine(bmiService.result());
            }
        }
    }
}
