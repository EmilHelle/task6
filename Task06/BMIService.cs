﻿using System;

namespace Task06
{
    class BMIService
    {
        public double BMI { get; set; }
        public BMIService(int weight, int height)
        {
            BMI = weight / Math.Pow(height / 100.0, 2);
        }

        public string result()
        {
            if (BMI < 18.5)
            {
                return "You are underweight";
            }
            else if (BMI >= 18.5 && BMI <= 24.9)
            {
                return "You have a normal weight";
            }
            else if (BMI >= 25 && BMI <= 29.9)
            {
                return "You are overweight";
            }
            else if (BMI >= 30)
            {
                return "You are obese";
            }
            else
            {
                return "...";
            }
        }

    }
}
